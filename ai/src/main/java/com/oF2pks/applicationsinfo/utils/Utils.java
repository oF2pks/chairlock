package com.oF2pks.applicationsinfo.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.WindowManager;
import android.widget.TextView;


import androidx.appcompat.app.AlertDialog;

import com.oF2pks.applicationsinfo.R;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

public class Utils {

    public static int getArrayLengthSafely(Object[] array) {
        return array == null ? 0 : array.length;
    }

    public static String aospString(Context ctx, String pkg, String r, String s) {
        Resources res = null;
        try {
            res = ctx.getPackageManager().getResourcesForApplication(pkg);
            int resourceId = res.getIdentifier(pkg+":string/"+r, null, null);
            if(0 != resourceId) {
                s = ctx.getPackageManager().getText(pkg, resourceId, null).toString();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return s;
    }
    @TargetApi(Build.VERSION_CODES.R)
    public static void usageDialog(final Context ctx) {
        TextView showText = new TextView(ctx);
        showText.setText("("
                + aospString(ctx, "com.android.settings","usage_access_description","Usage access allows an app to track what other apps you\\u2019re using and how often, as well as your carrier, language settings, and other details.")
                + ")\n\n"
                + aospString(ctx, "com.android.settings","write_settings","Modify system settings")
                + ": "
                + aospString(ctx, "com.android.settings","inactive_app_inactive_summary","Inactive. Tap to toggle.")
                +"\n\n" + ctx.getString(R.string.usage_info)
        );
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        Drawable ikn = null;
        try {
            ikn = ctx.getPackageManager().getApplicationIcon(ctx.getPackageName());
        } catch (PackageManager.NameNotFoundException e) {
            //e.printStackTrace();
        }
        builder.setView(showText)
                .setTitle(aospString(ctx, "com.android.settings","inactive_apps_title","Standby apps")
                        + ": "
                        + aospString(ctx, "com.android.settings","permit_usage_access","Permit usage access")
                        + " ?")
                .setIcon(ikn)
                .setCancelable(true)
                .setPositiveButton(aospString(ctx, "com.android.settings","display_category_title",ctx.getString(android.R.string.ok)), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ctx.startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS).setData(Uri.parse("package:" + ctx.getPackageName())));
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    public static int dpToPx(Context c, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, c.getResources().getDisplayMetrics());
    }

    public static int compareBooleans(boolean b1, boolean b2) {
        if (b1 && !b2) {
            return +1;
        }
        if (!b1 && b2) {
            return -1;
        }
        return 0;
    }

    public static String getFileContent(File file) {
        if (file.isDirectory())
            return "-1";

        try {
            Scanner scanner = new Scanner(file);
            String result = "";
            while (scanner.hasNext())
                result += scanner.next();
            return result;
        } catch (FileNotFoundException e) {
            return "-1";
        }
    }

    public static boolean isApi20() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH;
    }

    public static String getLaunchMode(int mode) {
        String[] zz={
                "Multiple",
                "Single top",
                "Single task",
                "Single instance",
                "Single instance per task",//31
        };
        if (mode<zz.length) return zz[mode];
        else return String.valueOf(mode);
    }

    public static String getOrientationString(int orientation) {
        String[] zz={
                "Unspecified",//-1 (0xffffffff)
                "Landscape",//0
                "Portrait",
                "User",
                "Behind",
                "Sensor",
                "No sensor",
                "Sensor landscape",
                "Sensor portrait",
                "Reverse landscape",
                "Reverse portrait",
                "Full sensor",//10
                "User landscape",
                "User portrait",
                "Full user",
                "Locked",
        };
    //-1 (0xffffffff)
        if ((orientation+1)<zz.length) return zz[orientation+1];
        else return String.valueOf(orientation+1);
    }

    public static String getSoftInputString(int flag) {
        StringBuilder builder = new StringBuilder();
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING) != 0)
            builder.append("Adjust nothing, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) != 0)
            builder.append("Adjust pan, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE) != 0)
            builder.append("Adjust resize, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED) != 0)
            builder.append("Adjust unspecified, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN) != 0)
            builder.append("Always hidden, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE) != 0)
            builder.append("Always visible, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) != 0)
            builder.append("Hidden, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE) != 0)
            builder.append("Visible, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED) != 0)
            builder.append("Unchanged, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED) != 0)
            builder.append("Unspecified, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION) != 0)
            builder.append("ForwardNav, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST) != 0)
            builder.append("Mask adjust, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_MASK_STATE) != 0)
            builder.append("Mask state, ");
        if ((flag & WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED) != 0)
            builder.append("Mode changed, ");

        checkStringBuilderEnd(builder);
        String result = builder.toString();
        return result.equals("") ? "null" : result;
    }

    public static String getServiceFlagsString(int flag) {
        StringBuilder builder = new StringBuilder();
        if ((flag & ServiceInfo.FLAG_STOP_WITH_TASK) != 0)
            builder.append("Stop with task, ");
        if ((flag & ServiceInfo.FLAG_ISOLATED_PROCESS) != 0)
            builder.append("Isolated process, ");

        if (Build.VERSION.SDK_INT >= 17) {
            if ((flag & ServiceInfo.FLAG_SINGLE_USER) != 0)
                builder.append("Single user, ");

            if (Build.VERSION.SDK_INT >= 24){
                if ((flag & ServiceInfo.FLAG_EXTERNAL_SERVICE)!= 0)
                    builder.append("External service, ");

                if (Build.VERSION.SDK_INT >= 29){
                    if ((flag & ServiceInfo.FLAG_USE_APP_ZYGOTE) != 0)
                        builder.append("Use app zygote, ");
                }
            }
        }

        checkStringBuilderEnd(builder);
        String result = builder.toString();
        return result.equals("") ? "\u2690" : "\u2691 "+result;
    }

    @TargetApi(Build.VERSION_CODES.Q)
    public static String getServiceForegroundString(int f) {
        StringBuilder builder = new StringBuilder();
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_MANIFEST) != 0)
            builder.append("allInManifest, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_NONE) != 0)
            builder.append("none, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC) != 0)
            builder.append("dataSync, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK) != 0)
            builder.append("mediaPlayback, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_PHONE_CALL) != 0)
            builder.append("phoneCall, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION) != 0)
            builder.append("location, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_CONNECTED_DEVICE) != 0)
            builder.append("connectedDevice, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PROJECTION) != 0)
            builder.append("mediaProjection, ");
        //30
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_CAMERA) != 0)
            builder.append("camera, ");
        if ((f & ServiceInfo.FOREGROUND_SERVICE_TYPE_MICROPHONE) != 0)
            builder.append("microphone, ");

        checkStringBuilderEnd(builder);
        return "\u269C" + builder.toString();
    }
    public static String getActivitiesFlagsString(int flag) {
        StringBuilder builder = new StringBuilder();
        if ((flag & ActivityInfo.FLAG_ALLOW_TASK_REPARENTING) != 0)
            builder.append("AllowReparenting, ");
        if ((flag & ActivityInfo.FLAG_ALWAYS_RETAIN_TASK_STATE) != 0)
            builder.append("AlwaysRetain, ");
        if ((flag & ActivityInfo.FLAG_AUTO_REMOVE_FROM_RECENTS) != 0)
            builder.append("AutoRemove, ");
        if ((flag & ActivityInfo.FLAG_CLEAR_TASK_ON_LAUNCH) != 0)
            builder.append("ClearOnLaunch, ");
        if ((flag & ActivityInfo.FLAG_ENABLE_VR_MODE) != 0)
            builder.append("EnableVR, ");
        if ((flag & ActivityInfo.FLAG_EXCLUDE_FROM_RECENTS) != 0)
            builder.append("ExcludeRecent, ");
        if ((flag & ActivityInfo.FLAG_FINISH_ON_CLOSE_SYSTEM_DIALOGS) != 0)
            builder.append("FinishCloseDialogs, ");
        if ((flag & ActivityInfo.FLAG_FINISH_ON_TASK_LAUNCH) != 0)
            builder.append("FinishLaunch, ");
        if ((flag & ActivityInfo.FLAG_HARDWARE_ACCELERATED) != 0)
            builder.append("HardwareAccel, ");
        if ((flag & ActivityInfo.FLAG_IMMERSIVE) != 0)
            builder.append("Immersive, ");
        if ((flag & ActivityInfo.FLAG_MULTIPROCESS) != 0)
            builder.append("Multiprocess, ");
        if ((flag & ActivityInfo.FLAG_NO_HISTORY) != 0)
            builder.append("NoHistory, ");
        if ((flag & ActivityInfo.FLAG_PREFER_MINIMAL_POST_PROCESSING) != 0)
            builder.append("MinPostProcessing, ");
        if ((flag & ActivityInfo.FLAG_RELINQUISH_TASK_IDENTITY) != 0)
            builder.append("RelinquishIdentity, ");
        if ((flag & ActivityInfo.FLAG_RESUME_WHILE_PAUSING) != 0)
            builder.append("Resume, ");
        if ((flag & ActivityInfo.FLAG_SINGLE_USER) != 0)
            builder.append("Single, ");
        if ((flag & ActivityInfo.FLAG_STATE_NOT_NEEDED) != 0)
            builder.append("NotNeeded, ");

        checkStringBuilderEnd(builder);
        String result = builder.toString();
        return result.equals("") ? "\u2690" : "\u2691 "+result;
    }

    public static String getProtectionLevelString(int level) {
        String protLevel = "????";
        switch (level & PermissionInfo.PROTECTION_MASK_BASE) {
            case PermissionInfo.PROTECTION_DANGEROUS:
                protLevel = "dangerous";
                break;
            case PermissionInfo.PROTECTION_NORMAL:
                protLevel = "normal";
                break;
            case PermissionInfo.PROTECTION_SIGNATURE:
                protLevel = "signature";
                break;
            case PermissionInfo.PROTECTION_SIGNATURE_OR_SYSTEM:
                protLevel = "signatureOrSystem";
                break;
            case PermissionInfo.PROTECTION_INTERNAL:
                protLevel = "internal";
                break;
        }//https://developer.android.com/reference/android/content/pm/PermissionInfo#getProtectionFlags()
        if (Build.VERSION.SDK_INT >= 23){
            if ((level  & PermissionInfo.PROTECTION_FLAG_PRIVILEGED) != 0)
                protLevel += "|privileged";
            if ((level  & PermissionInfo.PROTECTION_FLAG_PRE23) != 0)
                protLevel += "|pre23";
            if ((level  & PermissionInfo.PROTECTION_FLAG_INSTALLER) != 0)
                protLevel += "|installer";
            if ((level  & PermissionInfo.PROTECTION_FLAG_VERIFIER) != 0)
                protLevel += "|verifier";
            if ((level  & PermissionInfo.PROTECTION_FLAG_PREINSTALLED) != 0)
                protLevel += "|preinstalled";
            //24
            if ((level  & PermissionInfo.PROTECTION_FLAG_SETUP) != 0)
                protLevel += "|setup";
            //26
            if ((level  & PermissionInfo.PROTECTION_FLAG_RUNTIME_ONLY) != 0)
                protLevel += "|runtime";
            //27
            if ((level  & PermissionInfo.PROTECTION_FLAG_INSTANT) != 0)
                protLevel += "|instant";

            //https://developer.android.com/reference/kotlin/android/content/pm/PermissionInfo#getprotectionflags
            if (Build.VERSION.SDK_INT >= 28){
                //https://developer.android.com/reference/kotlin/android/R.attr.html#protectionLevel:kotlin.Int
                if ((level  & Integer.parseInt("4000", 16)) != 0)
                    protLevel += "|oemAllowlisted";
                if ((level  & Integer.parseInt("8000", 16)) != 0)
                    protLevel += "|vendorPrivileged";
            }
        }else if ((level & PermissionInfo.PROTECTION_FLAG_SYSTEM) != 0) {
            protLevel += "|system";
        }

        if ((level & PermissionInfo.PROTECTION_FLAG_DEVELOPMENT) != 0) {
            protLevel += "|development";
        }
        //21
        if ((level & PermissionInfo.PROTECTION_FLAG_APPOP) != 0) {
            protLevel += "|appop";
        }
        return protLevel;
    }

    public static String getPermissionFlagsString(int flags) {
        String permFlags = "";
        boolean b = false;
        if ((flags  & PermissionInfo.FLAG_COSTS_MONEY) != 0) {
            permFlags += "|CostsMoney";
            b =true;
        }
        if ((flags  & PermissionInfo.FLAG_HARD_RESTRICTED) != 0) {
            permFlags += "|HardRestricted";
            b =true;
        }
        if ((flags  & PermissionInfo.FLAG_IMMUTABLY_RESTRICTED) != 0) {
            permFlags += "|ImmutablyRestricted";
            b =true;
        }
        if ((flags  & PermissionInfo.FLAG_INSTALLED) != 0) {
            permFlags += "|Installed";
        }
        if ((flags  & PermissionInfo.FLAG_SOFT_RESTRICTED) != 0) {
            permFlags += "|SoftRestricted";
            b =true;
        }
        return (b ? "\u2691" : "\u2690")+permFlags;
    }
    public static String getFeatureFlagsString(int flags) {
        if (flags == FeatureInfo.FLAG_REQUIRED)
            return "Required";
        return "null";
    }

    public static String getInputFeaturesString(int flag) {
        String string = "";
        if ((flag & ConfigurationInfo.INPUT_FEATURE_FIVE_WAY_NAV) != 0)
            string += "Five way nav";
        if ((flag & ConfigurationInfo.INPUT_FEATURE_HARD_KEYBOARD) != 0)
            string += (string.length() == 0 ? "" : "|") + "Hard keyboard";
        return string.length() == 0 ? "null" : string;
    }

    public static void checkStringBuilderEnd(StringBuilder builder) {
        int length = builder.length();
        if (length > 2)
            builder.delete(builder.length() - 2, builder.length());
    }

    public static String getOpenGL(int reqGL){
            if (reqGL != 0) {
                return (short)(reqGL>>16)+"."+(short)reqGL;//Integer.toString((reqGL & 0xffff0000) >> 16);
            } else {
                return "1"; // Lack of property means OpenGL ES version 1
            }
    }
    public static String convertToHex(byte[] data) {//https://stackoverflow.com/questions/5980658/how-to-sha1-hash-a-string-in-android
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String signCert(Signature sign){
        String s= "";
        try {
            X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X.509")
                    .generateCertificate(new ByteArrayInputStream(sign.toByteArray()));

            s="\n"+cert.getIssuerX500Principal().getName()
                    +"\nCertificate fingerprints:"
                    +"\n"+"md5: "+Utils.convertToHex(MessageDigest.getInstance("md5").digest(sign.toByteArray()))
                    +"\n"+"sha1: "+Utils.convertToHex(MessageDigest.getInstance("sha1").digest(sign.toByteArray()))
                    +"\n"+"sha256: "+Utils.convertToHex(MessageDigest.getInstance("sha256").digest(sign.toByteArray()))
                    +"\n"+cert.toString()
                    +"\n"+(cert.getPublicKey().getAlgorithm())
                    +"---"+cert.getSigAlgName()+"---"+cert.getSigAlgOID()
                    +"\n"+(cert.getPublicKey())
                    +"\n";
        }catch (NoSuchAlgorithmException |CertificateException e) {
            return e.toString()+s;
        }
        return s;
    }

    public static Tuple apkPro(PackageInfo p){
        Signature[] z= p.signatures;
        String s="";
        X509Certificate c;
        Tuple t= new Tuple("","");
        try {
            for (Signature sg:z){
                c=(X509Certificate) CertificateFactory.getInstance("X.509")
                        .generateCertificate(new ByteArrayInputStream(sg.toByteArray()));
                s=c.getIssuerX500Principal().getName();
                if (!s.equals("")) t.setFirst(s);
                s=c.getSigAlgName();
                if (!s.equals("")) t.setSecond(s+"| "+Utils.convertToHex(MessageDigest.getInstance("sha256").digest(sg.toByteArray())));
            }

        }catch (NoSuchAlgorithmException | CertificateException e) {

        }
        return t;
    }


    public static Tuple permPro(PermissionInfo pi){
        Tuple t= new Tuple(pi.name,Utils.getProtectionLevelString(pi.protectionLevel));
/*        try {

        }catch (NoSuchAlgorithmException | CertificateException e) {

        }*/
        return t;
    }

    /**
     * Format xml file to correct indentation ...
     */
    public static String getProperXml(String dirtyXml) {
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new ByteArrayInputStream(dirtyXml.getBytes("utf-8"))));

            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",
                    document,
                    XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);

            transformer.transform(new DOMSource(document), streamResult);

            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String simpleDate(String date) {
        int index = date.indexOf(" GMT");
        if(index > -1) return date.substring(date.length()-4) + " " + date.substring(0, index);
        return date;
    }
}
