package com.oF2pks.chairlock;

import static com.oF2pks.applicationsinfo.utils.Utils.usageDialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.oF2pks.applicationsinfo.DetailActivity;
import com.oF2pks.applicationsinfo.DetailFragment;

public class LaunchActivity extends AppCompatActivity implements LaunchCallbacks {

    private boolean mIsDualPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().build());
        }
        StrictMode.allowThreadDiskReads();
        StrictMode.allowThreadDiskWrites();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            SharedPreferences settings = getApplication().getSharedPreferences("appsInfo-SharedPreferences", 0);
            if (settings.getBoolean("primLaunch", true)) {
                usageDialog(this);
                settings.edit().putBoolean("primLaunch", false).commit();
            }
        }

        setContentView(R.layout.activity_launch);
        mIsDualPane = findViewById(R.id.item_detail_container) != null;

        //Show an art when no fragment is showed, we make sure no detail fragment is present.
        if (mIsDualPane && getSupportFragmentManager().findFragmentByTag(DetailFragment.FRAGMENT_TAG) == null) {
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.icon_art);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            ((FrameLayout) findViewById(R.id.item_detail_container)).addView(imageView);
        }
    }

    @Override
    public void onItemSelected(String packageName , String permPackages) {
        if (mIsDualPane) {
            //Hide art when a fragment is showed.
                ((FrameLayout) findViewById(R.id.item_detail_container)).removeAllViews();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.item_detail_container, DetailFragment.getInstance(packageName), DetailFragment.FRAGMENT_TAG)
                    .commit();
        } else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailFragment.EXTRA_PACKAGE_NAME, packageName);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_launch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_about) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.about)
                    .setView(getLayoutInflater().inflate(R.layout.about_chairlock_message, null))
                    .setNegativeButton(android.R.string.ok, null)
                    .setIcon(R.drawable.ic_launcher_chairlock)
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
